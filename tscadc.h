#ifndef TSCADC_H
#define TSCADC_H

#include <sys/types.h>

void tscadc_open(void);
int tscadc_close(void);
volatile u_int32_t *tscadc_clkdiv(void);
void tscadc_nodelay(void);
void tscadc_dump(void);

void tscadc_enable_channel(size_t device, size_t channel);
void tscadc_set_buffer_length(size_t device, size_t length);
void tscadc_enable_buffer(size_t device);

#endif

