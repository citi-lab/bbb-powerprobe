#include <fstream>
#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>

#include "mem.h"
#include "gpio.h"

#define GPIO0_BASE 0x44e07000
#define GPIO0_SIZE 0x1000

#define GPIO1_BASE 0x4804c000
#define GPIO1_SIZE 0x1000

#define GPIO2_BASE 0x481ac000
#define GPIO2_SIZE 0x1000

#define GPIO3_BASE 0x481ae000
#define GPIO3_SIZE 0x1000

#define GPIO_SYSCONFIG    (0x010>>2)
#define GPIO_CTRL         (0x130>>2)
#define GPIO_OE           (0x134>>2)
#define GPIO_DATAIN       (0x138>>2)
#define GPIO_DATAOUT      (0x13c>>2)
#define GPIO_CLEARDATAOUT (0x190>>2)
#define GPIO_SETDATAOUT   (0x194>>2)

static volatile u_int32_t *gpio[4];
const size_t sizes[] = {GPIO0_SIZE, GPIO1_SIZE, GPIO2_SIZE, GPIO3_SIZE};

static void gpio_export(size_t pin)
{
    std::cout << "Export " << pin << std::endl;
    std::ofstream file("/sys/class/gpio/export", std::ios::binary);
    file << pin;
}

void gpio_open(void)
{
    gpio[0] = (volatile u_int32_t*) mem_map(GPIO0_BASE, GPIO0_SIZE);
    gpio[1] = (volatile u_int32_t*) mem_map(GPIO1_BASE, GPIO1_SIZE);
    gpio[2] = (volatile u_int32_t*) mem_map(GPIO2_BASE, GPIO2_SIZE);
    gpio[3] = (volatile u_int32_t*) mem_map(GPIO3_BASE, GPIO3_SIZE);

    //gpio[3][GPIO_SYSCONFIG] = (1u << 3);
}

void gpio_set_out(size_t pin, u_int8_t defaultValue)
{
    volatile u_int32_t *tmp = gpio[pin/32];
    u_int32_t mask = (1u << (pin%32));

    gpio_export(pin);

    tmp[GPIO_OE] &= ~mask;
    gpio_write(pin, defaultValue);
}

void gpio_set_in(size_t pin)
{
    volatile u_int32_t *tmp = gpio[pin/32];
    u_int32_t mask = (1u << (pin%32));

    gpio_export(pin);

    tmp[GPIO_OE] |= mask;
}

void gpio_write(size_t pin, u_int8_t value)
{
    volatile u_int32_t *tmp = gpio[pin/32];
    u_int32_t mask = (1u << (pin%32));

    if(value & 1)
        tmp[GPIO_SETDATAOUT] = mask;
    else
        tmp[GPIO_CLEARDATAOUT] = mask;
}

u_int8_t gpio_read(size_t pin)
{
    return (gpio[pin/32][GPIO_DATAIN] >> (pin%32)) & 1;
}

void gpio_dump(size_t bank)
{
    volatile u_int32_t *ptr = gpio[bank];
    size_t size = sizes[bank];

    for(size_t offset = 0; offset < (size>>2); ++offset)
        printf("[%03x] %08x\n", offset<<2, ptr[offset]);
}

void gpio_dump_pin(size_t pin)
{
    size_t bank = pin/32;
    printf("[SYSC] %08x\n", gpio[bank][GPIO_SYSCONFIG]);
    printf("[CTRL] %08x\n", gpio[bank][GPIO_CTRL]);
    printf("[OE  ] %08x\n", gpio[bank][GPIO_OE]);
    printf("[OUT ] %08x\n", gpio[bank][GPIO_DATAOUT]);
}

