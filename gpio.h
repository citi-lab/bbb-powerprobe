#ifndef GPIO_H
#define GPIO_H

void gpio_open(void);
void gpio_set_out(size_t pin, u_int8_t value);
void gpio_set_in(size_t pin);
void gpio_write(size_t pin, u_int8_t value);
u_int8_t gpio_read(size_t pin);

void gpio_dump(size_t bank);
void gpio_dump_pin(size_t pin);

#endif

