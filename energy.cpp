#include <iostream>
#include <sstream>

#include <fcntl.h>
#include <unistd.h>

#include "energy.h"
#include "gpio.h"

#define PIN_MSP_RDY 49
#define PIN_BBB_RDY 115

static int fd;

void energy_init(size_t device)
{
    gpio_set_in(PIN_MSP_RDY);
    gpio_set_out(PIN_BBB_RDY, 0);

    std::ostringstream oss;
    oss << "/dev/iio:device" << device;

    fd = open(oss.str().c_str(), O_RDONLY);
}

void energy_close(void)
{
    close(fd);
}

std::vector<measure_t> energy_measure_single(size_t maxcount)
{
    const size_t bytecount = ENERGY_BUFSIZE * sizeof(u_int16_t);

    std::vector<measure_t> measures(maxcount);

    while(!gpio_read(PIN_MSP_RDY));
    gpio_write(PIN_BBB_RDY, 1);
    while(gpio_read(PIN_MSP_RDY));
    gpio_write(PIN_BBB_RDY, 0);

    size_t i = 0;
    for(; i < maxcount && !gpio_read(PIN_MSP_RDY); ++i)
    {
        read(fd, measures[i].data, bytecount);
        measures[i].when = std::chrono::system_clock::now();
    }

    if(i == maxcount) // memory overflow
    {
        std::cerr << "energy_measure_single: memory overflow" << std::endl;
        while(!gpio_read(PIN_MSP_RDY));
    }
    else
        measures.resize(i);

    gpio_write(PIN_BBB_RDY, 1);
    while(gpio_read(PIN_MSP_RDY));
    gpio_write(PIN_BBB_RDY, 0);

    return measures;
}

