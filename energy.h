#ifndef ENERGY_H
#define ENERGY_H

#include <chrono>
#include <vector>

#define ENERGY_BUFSIZE 128

struct measure_t
{
    std::chrono::system_clock::time_point when;
    u_int16_t data[ENERGY_BUFSIZE];
};

void energy_init(size_t device);
void energy_close(void);
std::vector<measure_t> energy_measure_single(size_t maxcount);

#endif

