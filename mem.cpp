#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

static int mem_fd;

int mem_open(void)
{
    if((mem_fd = open("/dev/mem", O_RDWR|O_SYNC)) < 0)
    {
        printf("can't open /dev/mem \n");
        exit(EXIT_FAILURE);
    }
    return mem_fd;
}

int mem_close(void)
{
    return close(mem_fd);
}

void *mem_map(size_t base, size_t size)
{
    void *tmp = mmap(
        0,
        size,
        PROT_READ|PROT_WRITE,
        MAP_SHARED,
        mem_fd,
        base
    );

    if(tmp == MAP_FAILED)
    {
        printf("mmap error %d\n", (int)tmp);
        exit(EXIT_FAILURE);
    }

    return tmp;
}

