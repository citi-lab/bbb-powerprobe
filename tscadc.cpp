#include <fstream>
#include <sstream>
#include <string>

#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>

#include "mem.h"

#define TSCADC_BASE 0x44e0d000
#define TSCADC_SIZE 0xfc

#define ADC_CLKDIV      (0x4c>>2)
#define STEPDELAY1      (0x68>>2)

static volatile u_int32_t *tscadc;
const std::string iio_dir("/sys/bus/iio/devices/");

void tscadc_open(void)
{
    tscadc = (volatile unsigned int*) mem_map(TSCADC_BASE, TSCADC_SIZE);
}

volatile u_int32_t *tscadc_clkdiv(void)
{
    return tscadc + ADC_CLKDIV;
}

void tscadc_nodelay(void)
{
    for(size_t i = 0; i < 16; ++i)
        tscadc[STEPDELAY1 + (i<<1)] = 0;
}

void tscadc_dump(void)
{
    for(size_t offset = 0; offset < (TSCADC_SIZE>>2); ++offset)
        printf("[%02x] %08x\n", offset<<2, tscadc[offset]);
}

void tscadc_enable_channel(size_t device, size_t channel)
{
    std::ostringstream oss;
    oss << iio_dir << "iio:device" << device << "/scan_elements/in_voltage" << channel << "_en";

    std::ofstream os(oss.str());
    os << 1;
}

void tscadc_set_buffer_length(size_t device, size_t length)
{
    std::ostringstream oss;
    oss << iio_dir << "iio:device" << device << "/buffer/length";

    std::ofstream os(oss.str());
    os << length;
}

void tscadc_enable_buffer(size_t device)
{
    std::ostringstream oss;
    oss << iio_dir << "iio:device" << device << "/buffer/enable";

    std::ofstream os(oss.str());
    os << 1;
}
