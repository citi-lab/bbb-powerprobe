#include <iostream>
#include <sstream>
#include <vector>

#include <fcntl.h>
#include <unistd.h>

#include "mem.h"
#include "gpio.h"
#include "tscadc.h"
#include "energy.h"

/* Available gpio (default mux=gpio):
 * https://vadl.github.io/images/bbb/P9Header.png
 * https://stackoverflow.com/questions/12025119/beaglebone-gpio-input-not-working
 */

const size_t DEVICE  = 0;
const size_t CHANNEL = 5;

static void print_measures(const std::vector<measure_t> &measures)
{
    auto initclock = measures[0].when;

    for(const auto &measure : measures)
    {
        std::chrono::duration<double> t = measure.when - initclock;
        std::cout << t.count() << '|';
        for(size_t i = 0; i < ENERGY_BUFSIZE; ++i)
        {
            std::cout << measure.data[i];
            if(i+1 != ENERGY_BUFSIZE)
                std::cout << ',';
        }
        std::cout << std::endl;
    }
}

int main(int argc, char *argv[])
{
    if(argc < 2)
    {
        std::cerr << "Usage: ./sampler n_samples" << std::endl;
        return EXIT_FAILURE;
    }

    size_t samplecount;
    std::istringstream samplecount_iss(argv[1]);
    samplecount_iss >> samplecount;

    std::cout << "mem" << std::endl;
    mem_open();
    std::cout << "gpio" << std::endl;
    gpio_open();
    std::cout << "adc" << std::endl;
    tscadc_open();

    *tscadc_clkdiv() = 0;
    tscadc_nodelay();
    // Now, sampling freq is approx. 25.6 kHz

    tscadc_enable_channel(DEVICE, CHANNEL);
    tscadc_set_buffer_length(DEVICE, ENERGY_BUFSIZE);
    tscadc_enable_buffer(DEVICE);

    std::cout << "energy" << std::endl;
    energy_init(DEVICE);

    std::cout << "ok" << std::endl;

    for(size_t i = 0; i < samplecount; ++i)
    {
        auto measures = energy_measure_single(512);
        print_measures(measures);

        if(i+1 != samplecount)
            std::cout << '#' << std::endl;
    }

    energy_close();
    mem_close();

    return EXIT_SUCCESS;
}

