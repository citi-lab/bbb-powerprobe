#ifndef MEM_H
#define MEM_H

int mem_open(void);
int mem_close(void);
void *mem_map(size_t base, size_t size);

#endif

